<?php

session_start();
session_name("session_name");

include './config.php';
include './controllers/Controller.php';
include './controllers/Mobile_Detect.php';

$controller = new Controller();

if (isset($_REQUEST['auth_type']) && isset($_REQUEST['kw']) && isset($_REQUEST['sc'])) {
    $auth_type = strtolower($_REQUEST['auth_type']);
} else {
    $auth_type = isset($_REQUEST['act']) ? $_REQUEST['act'] : 'home';
}

switch ($auth_type) {
    case 'home': $controller->showHome();
        break;
    case 'submit-phone': $controller->submitPhone();
        break;
    case 'clicktosms': $controller->sendSMS();
        break;
    default: echo '<h1>No Proper Action Selected!</h1>'; die;
        break;
}

extract($controller->vars);

if(!isset($view) || !file_exists("./views/pages/$view.php")){
    die("<h1>No Page Is Set!</h1>");
}

include "./views/$layout.php";
