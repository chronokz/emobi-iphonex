  <div class="page1">
    <div class="header">
      <h2 class="float-right">Oto<br>twoja<br>szansa!</h2>
      <h1>Wygraj<br>iPhone</h1>
    </div><img src="img/img-header.png" srcset="img/img-header@2x.png 2x, img/img-header@3x.png 360w" class="logo">
    <div class="slog">Aby wygrać odpowiedz na pytanie:</div>
    <div class="vote">
      <h3>Kto jest współzałożycielem firmy Apple?</h3>
      <a href="<?=ORDER_LINK?>" class="btn btn-danger float-left">Steve<br>Jobs</a>
      <a href="<?=ORDER_LINK?>" class="btn btn-primary float-right">Bill<br>Gates</a>
      <div class="clearfix"></div>
    </div>
  </div>
  <?php
    include './views/inc/secure.html';
    include './views/inc/text.html';
    include './views/inc/impress.html';
    include './views/inc/features.html';
    include './views/inc/apps.html';
    include './views/inc/footer.html';
  ?>