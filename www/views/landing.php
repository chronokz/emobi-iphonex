<!DOCTYPE html>
<html lang="en">
  <head>
    <title>iPhone X lite version (PO)</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=360">
    <base href="resources/">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>
    <div class="body">
      <?php include './views/pages/'.$view.'.php'; ?>
    </div>
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>