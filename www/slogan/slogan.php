<?php
	session_start();

	$msisdn = isset($_SESSION['msisdn']) ? $_SESSION['msisdn']:"";

	if (!file_exists('data')) {
	    mkdir('data', 0777, true);
	}
	if(!empty($_POST) && isset($_POST)){
		$f = fopen("data/".date("Y-m-d").".csv", "a");

		fputcsv($f, array("number", "'".strval($msisdn), "slogan text", $_POST["slogan"]));
		fclose($f);
		header('location:/slogan-pg2.php');
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>
	Najlepsza i najbardziej kreatywna odpowiedź wygrywa!
  </title>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; minimum-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
  <meta name="format-detection" content="telephone=no">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="slogan_style.css" type="text/css" media="screen" />
</head>
<body>
    <div id="main">
        <div class="lp">
                <div class="text-wrap">
                    <img style="width:100%" src="images/prizehub-logo.png" class="aligncenter">
                    <div id="tell-us">
                        <p>W polu poniżej napisz dlaczego zasługujesz na wygranie nagrody głównej.</p>
                    </div>
                    <br>
                    <p><span id="the-best">Najlepsza i najbardziej kreatywna odpowiedź wygrywa!</span></p>
                    <br>
                    <p><span id="enter">Minimalna długość tekstu; 100, max. długość; 600 znaków.</span></p>
                </div>
                <div id="form">
                    <form action="slogan-pg2.php" method="post" id="usrform">
                        <textarea maxlength="600" minlength="100" name="slogan" rows="12" cols="28" form="usrform" placeholder="Wpisz swoją odpowiedź poniżej:" style="padding: 10px;width: 100%;"></textarea>
                        <br><br>
                        <input type="submit" value="WYŚLIJ">
                </form>

                </div>
                <div id="footer">
                    <div class="foot-menu">
                        <ul>
                            <li><a target="_blank" href="http://mobilehub.mobi/PL/regulamin.html" id="TCs" data-content-name="footer-term" data-content-type="link">Regulamin</a></li>
                            <li><a target="_blank" href="http://mobilehub.mobi/PL/obslugiwane_telefony.html" id="compatibility" data-content-name="footer-compatibility" data-content-type="link">Obsługiwane telefony</a></li>
                            <li><a target="_blank" href="http://mobilehub.mobi/PL/polityka_prywatnosci.html" id="privacy" data-content-name="footer-policy" data-content-type="link">Polityka prywatności</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
