<?php

class Controller {

    public $vars = array(
        'view' => 'home',
        'layout' => 'landing'
    );

    function __construct() {

    }

    public function showHome() {
        // url to next page - submit phone
        $this->vars['URL'] = currentUrl().http_build_query(array_merge($_GET, array("act"=>"submit-phone")));

        $this->vars['view'] = 'home';
    }

    public function submitPhone() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_REQUEST['msisdn']) && $_REQUEST['msisdn'] !== '') {

            // json params
            $params = json_encode(array(
                'cid' => isset($_REQUEST['cid'])?$_REQUEST['cid']:'00000000-0000-0000-0000-000000000000',
                'rotate_link' => isset($_REQUEST['rotate_link'])?$_REQUEST['rotate_link']:'unknown',
                'tsp_link' => 'http://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],
                'club' => isset($_REQUEST['club'])?$_REQUEST['club']:'unknown',
                'country' => strtoupper('pl'),
                'params' => $_SERVER['QUERY_STRING'],
                'msisdn' => $_REQUEST['msisdn'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT']
            ));
            send_entered_number('http://api.emobi-sys.com:8091/v1/affiliates/entered_number', $params);

            $_SESSION['msisdn'] = $_REQUEST['msisdn'];
            echo "<script>window.location.href='".ORDER_LINK."'</script>";
        } else {
            $this->vars['view'] = 'submit-phone';
        }
    }

    public function sendSMS()
    {
        $_REQUEST['kw'] = isset($_REQUEST['kw'])?$_REQUEST['kw']:'';
        $_REQUEST['auth_id'] = isset($_REQUEST['auth_id'])?$_REQUEST['auth_id']:'';
        $_REQUEST['sc'] = isset($_REQUEST['sc'])?$_REQUEST['sc']:'';

        $detect = new Mobile_Detect;
        if ( $detect->isMobile() ) {
            $this->vars['KW_FORMATTED'] = $_REQUEST['kw'];
        } else {
            $this->vars['KW_FORMATTED'] = $_REQUEST['kw'].' '.$_REQUEST['auth_id'];
        }

        $this->vars['KW'] = $_REQUEST['kw'].' '.$_REQUEST['auth_id'];
        $this->vars['SC'] = $_REQUEST['sc'];
        $this->vars['symbol'] = getSMSTextConcatenator();

        $this->vars['view'] = 'send-sms';
    }

}

function send_entered_number($url, $payload) {
    $cmd = "curl -X POST -H 'Content-Type: application/json'";
    $cmd.= " -d '" . $payload . "' " . "'" . $url . "'";
    exec($cmd, $output, $exit);
    return $exit == 0;
}

    function getSMSTextConcatenator() {
        if (is_android2()) {
            $symbol = '';
        } elseif (is_imobile()) {
            if (is_ios8min()) {
                $symbol = '&';
            }
        } else {
            $symbol = '?';
        }
        return $symbol;
    }

    function is_android2() {
        return preg_match('/Android 2/i', $_SERVER['HTTP_USER_AGENT']);
    }

    function is_imobile() {
        return preg_match('/iphone|ipod|ipad/i', $_SERVER['HTTP_USER_AGENT']);
    }

    function is_ios8min() {
        if ($result = preg_match('/OS ([0-9]+)/i', $_SERVER['HTTP_USER_AGENT'], $matches))
            $result = $matches[1] >= 8;
        return $result;
    }

    function currentUrl() {
        $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
        $host     = $_SERVER['HTTP_HOST'];
        $script   = $_SERVER['SCRIPT_NAME'];

        return $protocol . '://' . $host . $script . '?';
    }
