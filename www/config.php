<?php

// Define display error
// error_reporting(0);
error_reporting(E_ALL);

global $configs;

$configs = array();

$configs['isoCountryCode'] = 'pl';
$configs['orderContext'] = 'emobi';
$configs['currency'] = 'PLN';
$configs['amount'] = '615';
$configs['productReference'] = 'pl_prizehub_club';
$configs['productType'] = 'SUBSCRIPTION';
$configs['operator'] = '';

define('ADDR', "http://api.emobi-sys.com:8091/v1/purchase/order");
define('ORDER_LINK', ADDR . "?" . http_build_query($configs));

/* Global function */
if (!function_exists('getallheaders')) 
{ 
    function getallheaders() 
    { 
           $headers = ''; 
       foreach ($_SERVER as $name => $value) 
       { 
           if (substr($name, 0, 5) == 'HTTP_') 
           { 
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
           } 
       } 
       return $headers; 
    } 
} 
